<?php
/**
 * @file
 * drush integration.
 */

require_once 'site_cleanup.inc';
require_once 'site_cleanup.items.inc';

/**
 * Implements hook_drush_command().
 */
function site_cleanup_drush_command() {
  $items = array();

  $items['site-cleanup'] = array(
    'description' => "Performs a site-cleanup. Caution, this command might make your site look a bit empty.",
    'drupal dependencies' => array('site_cleanup'),
    'callback' => 'drush_site_cleanup',
    'options' => array('all' => 'Process all the items below.'),
  );

  // Populate options.
  foreach (_site_cleanup_drush_get_options() as $item) {
    $items['site-cleanup']['options'][_site_cleanup_drush_normalize_string($item['groupheader'])] = 'Process ' . $item['groupheader'];
  }

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function site_cleanup_drush_help($section) {
  switch ($section) {
    case 'drush:site-cleanup':
      $message = dt("Performs a site-cleanup. Caution, this command might make your site look a bit empty.");
      break;

    default:
      $message = dt('Try --help');
      break;

  }
  return $message;
}

/**
 * Main function.
 */
function drush_site_cleanup() {

  $batch = _site_cleanup_create_batch();
  // Check which options were selected and add to the batch list.
  foreach (_site_cleanup_drush_get_options() as $item) {
    if (drush_get_option(_site_cleanup_drush_normalize_string($item['groupheader'])) || drush_get_option('all')) {
      $batch['operations'][] = array(
        'site_cleanup_bulk_update_batch_process',
        array(
          $item['batch_update_callback'],
          $item,
        ),
      );
    }
  }

  batch_set($batch);

  $batch =& batch_get();
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();
}

/**
 * Cleans the string, removing capitals, spaces.
 */
function _site_cleanup_drush_normalize_string($string) {
  $string = preg_replace('/[^a-z0-9 \.]/i', '', strtolower($string));
  $string = str_replace(' ', '-', $string);
  return $string;
}

/**
 * Load items from site_cleanup.items.inc.
 */
function _site_cleanup_drush_get_options() {
  $options = site_cleanup_site_cleanup_default_items();

  /* @Todo Add support to use sub-options. At the moment its not possible
   * to choose which taxonomy to delete
   * foreach ($options as $key => $item) {
   * }
   */

  return $options;
}
