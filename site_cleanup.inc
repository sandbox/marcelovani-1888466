<?php
/**
 * @file
 * Site Cleanup Module functions.
 */

/**
 * Batch processing callback for nodes.
 */
function site_cleanup_bulk_update_batch_process_node(&$context) {
  // Setting used to avoid deleting nodes created during site install.
  $delete_default_nodes  = $context['settings']['sub_options']['delete_default_nodes']['value'];
  // Currently there are only 4 pages created durint site install.
  $default_nodes_count = 4;

  // Set count/current.
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['count'] = 0;
    if ($delete_default_nodes) {
      $context['sandbox']['current'] = 0;
    }
    else {
      $context['sandbox']['current'] = $default_nodes_count;
    }
  }

  // Get nodes.
  $query = db_select('node', 'n');
  $query->addField('n', 'nid');
  $query->condition('n.nid', $context['sandbox']['current'], '>');
  $query->addTag('site_cleanup_bulk_update');
  $query->addMetaData('entity', 'node');

  // Get the total amount of items to process.
  if (!isset($context['sandbox']['total'])) {
    $context['sandbox']['total'] = $query->countQuery()->execute()->fetchField();

    // If there are no nodes to update, the stop immediately.
    if (!$context['sandbox']['total']) {
      $context['finished'] = 1;
      drupal_set_message(t('Nodes deleted.'));
      return;
    }
  }

  $query->range(0, 10);
  $nids = $query->execute()->fetchCol();

  // Delete nodes.
  node_delete_multiple($nids);

  $context['sandbox']['count'] += count($nids);
  $context['sandbox']['current'] = max($nids);
  $context['message'] = t('Deleting nodes @nid.', array('@nid' => implode(', ', $nids)));

  if ($context['sandbox']['count'] != $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}

/**
 * Batch processing callback for taxonomy terms.
 */
function site_cleanup_bulk_update_batch_process_taxonomy(&$context) {
  // Setup count/current
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['count'] = 0;
    $context['sandbox']['current'] = 0;
  }

  // Get the total amount of items to process.
  if (!isset($context['sandbox']['total'])) {
    $context['sandbox']['total'] = 1;

    // If there are no menus to update, the stop immediately.
    if (!$context['sandbox']['total']) {
      $context['finished'] = 1;
      return;
    }
  }

  // Populate array of vocabularies to be deleted.
  $vids_delete = array();
  $vids_keep = array();
  foreach ($context['settings']['sub_options'] as $key => $item) {
    if ($item['value'] == TRUE) {
      if ($item['type'] == 'vocabulary') {
        $vids_delete[] = $item['vid'];
      }
    }
  }
  // Populate array of terms to be deleted.
  $tids_delete = array();
  foreach ($context['settings']['sub_options'] as $key => $item) {
    if ($item['value'] == TRUE) {
      if ($item['type'] == 'term') {
        if (in_array($item['vid'], $vids_delete)) {
          $tids_delete[] = $item['tid'];
        }
      }
    }
    else {
      // Avoid deleted vocabularies when not all the child terms were selected.
      $vids_keep[] = $item['vid'];
    }
  }

  // Remove vids from list ot vids to be deleted.
  foreach ($vids_keep as $key => $item) {
    $found = array_search($item, $vids_delete);
    if ($found !== FALSE) {
      unset($vids_delete[$found]);
    }
  }

  // Delete terms.
  // Terms deleting terms that have children will delete the children too.
  foreach ($tids_delete as $key => $tid) {
    // Drupal_set_message('deleted tid ' . $tid);.
    taxonomy_term_delete($tid);
  }

  // Delete vocabularies.
  foreach ($vids_delete as $key => $vid) {
    // Drupal_set_message('deleted vid ' . $vid);.
    taxonomy_vocabulary_delete($vid);
  }

  $context['message'] = t('Deleting terms and vocabularies.');
  $context['finished'] = 1;
  drupal_set_message(t('Taxonomy updated.'));
}

/**
 * Batch processing callback for users.
 */
function site_cleanup_bulk_update_batch_process_user(&$context) {
  // Setup count/current.
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['count'] = 0;
    // Skip admin user (1).
    $context['sandbox']['current'] = 1;
  }

  // Get users.
  $query = db_select('users', 'u');
  $query->addField('u', 'uid');
  $query->condition('u.uid', $context['sandbox']['current'], '>');
  $query->orderBy('u.uid');
  $query->addTag('site_cleanup_bulk_update');
  $query->addMetaData('entity', 'user');

  // Get the total amount of items to process.
  if (!isset($context['sandbox']['total'])) {
    $context['sandbox']['total'] = $query->countQuery()->execute()->fetchField();

    // If there are no users to update, the stop immediately.
    if (!$context['sandbox']['total']) {
      $context['finished'] = 1;
      drupal_set_message(t('Users deleted.'));
      return;
    }
  }

  $query->range(0, 10);
  $uids = $query->execute()->fetchCol();

  // Delete users.
  user_delete_multiple($uids);

  $context['sandbox']['count'] += count($uids);
  $context['sandbox']['current'] = max($uids);
  $context['message'] = t('Deleting user @uid.', array('@uid' => implode(', ', $uids)));

  if ($context['sandbox']['count'] != $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}

/**
 * Batch processing callback; Generate aliases for main menu.
 */
function site_cleanup_bulk_update_batch_process_main_menu(&$context) {
  return _site_cleanup_bulk_update_batch_process_menu(&$context);
}

/**
 * Batch processing callback; Generate aliases for secondary menu.
 */
function site_cleanup_bulk_update_batch_process_secondary_menu(&$context) {
  return _site_cleanup_bulk_update_batch_process_menu(&$context);
}

/**
 * Common batch processing callback for menus.
 */
function _site_cleanup_bulk_update_batch_process_menu(&$context) {
  // Get menu name from the item settings.
  $menu_name  = $context['settings']['menu_name'];

  // Setup count/current
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['count'] = 0;
    $context['sandbox']['current'] = 0;
  }

  // Get main menu items.
  $menu_query = db_select('menu_links', 'm');
  $menu_query->addField('m', 'mlid');
  $menu_query->addField('m', 'link_title');
  $menu_query->condition('m.menu_name', $menu_name, '=');
  $menu_query->condition('m.mlid', $context['sandbox']['current'], '>');
  $menu_query->orderBy('m.weight');
  $menu_query->addTag('site_cleanup_bulk_update');
  $menu_query->addMetaData('entity', 'menu');

  // Get the total amount of items to process.
  if (!isset($context['sandbox']['total'])) {
    $context['sandbox']['total'] = $menu_query->countQuery()->execute()->fetchField();

    // If there are no menus to update, the stop immediately.
    if (!$context['sandbox']['total']) {
      $context['finished'] = 1;
      return;
    }
  }

  // Get terms to fill in the menus.
  $term_query = db_select('taxonomy_term_data', 't');
  $term_query->addField('t', 'tid');
  $term_query->addField('t', 'name');

  // Use different vocabulary for each menu.
  switch ($menu_name) {
    case 'main-menu':
      $term_query->condition('t.vid', '3', '>=');
      break;

    case 'secondary-menu':
      $term_query->condition('t.vid', '1', '>=');
      break;

  }

  // Get terms.
  $term_query->orderBy('t.tid');
  $term_query->range(0, $context['sandbox']['total'] + 1);
  $term_query->addTag('site_cleanup_bulk_update');
  $term_query->addMetaData('entity', 'term');
  $result = $term_query->execute();

  // Transform result into array.
  $terms = array();
  $i = 0;
  foreach ($result as $term) {
    $terms['tid'][$i] = $term->tid;
    $terms['name'][$i] = $term->name;
    $i++;
  }

  // Get menus.
  $menu_query->range(0, 10);
  $result = $menu_query->execute();

  // Transform result into array.
  $i = 0;
  foreach ($result as $menu) {
    $i++;
    $link = menu_link_load($menu->mlid);
    $link['link_title'] = $terms['name'][$i];
    $link['link_path'] = 'taxonomy/term/' . $terms['tid'][$i];

    // Save menu.
    menu_link_save($link);
  }

  $context['finished'] = 1;
  drupal_set_message(t('Menus have been updated.'));
}

/**
 * Batch processing callback for languages.
 */
function site_cleanup_bulk_update_batch_process_language(&$context) {
  drupal_set_message(t('Language not implemented yet.'));
}


/**
 * Batch processing for aliases.
 */
function site_cleanup_bulk_update_batch_process_aliases(&$context) {
  // Code below is from pathauto module.
  db_delete('url_alias')->execute();
  drupal_set_message(t('All of your path aliases have been deleted.'));
}

/**
 * Batch processing for xmlsitemap.
 */
function site_cleanup_bulk_update_batch_process_xmlsitemap(&$context) {
  db_delete('xmlsitemap')->execute();
  drupal_set_message(t('XML Sitemap have been deleted.'));
}

/**
 * Batch processing for logs.
 */
function site_cleanup_bulk_update_batch_process_logs(&$context) {
  db_delete('watchdog')->execute();
  drupal_set_message(t('Logs have been deleted.'));
}

/**
 * Batch processing for metatag.
 */
function site_cleanup_bulk_update_batch_process_metatag(&$context) {
  db_delete('metatag')->execute();
  drupal_set_message(t('Metatags have been deleted.'));
}

/**
 * Batch processing for dart.
 */
function site_cleanup_bulk_update_batch_process_dart(&$context) {
  db_delete('dart_taxonomy_map')->execute();
  drupal_set_message(t('Darts have been deleted.'));
}
