#!/bin/bash
# This script downloads the DB, enable and run Site Cleanup

# enable site_cleanup module
echo Enabling Site Cleanup Module
drush en -y site_cleanup;

# clean up site
echo Cleaning up site
drush site-cleanup --all;

# make the archive
# drush archive-dump --destination=/tmp/site.tag.gz

echo Finished.
