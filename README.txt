Site Cleanup
-----------------
Remove and define skeleton nodes, users, terms, menus, etc.
Use with caution, after this your site might look a bit empty.

Installation
------------
Install the module as usual

Usage
-----
Visit /admin/config/system/site_cleanup
Choose the items you want to delete.

Drush
  Examples:  
    Cleaning menus: # drush site-cleanup --main-menu --secondary-menu
    Help: # drush site-cleanup --help
    
Bash Script
Please see an example on site_cleanup.example.sh
To run, make sure the file can be executed: chmod +x site_cleanup.example.sh
