<?php
/**
 * @file
 * Site Cleanup items.
 */

include_once "site_cleanup.module";

/**
 * Implements hook_site_cleanup_default_items().
 */
function site_cleanup_site_cleanup_default_items() {
  $default_items = array();

  // Nodes.
  $item = array();
  $item['module'] = 'node';
  $item['groupheader'] = t('Content');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_node';
  $item['sub_options'] = array(
    'delete_default_nodes' => array(
      'title' => 'Remove content created during Site Install',
      'value' => FALSE,
      'parent' => $item['batch_update_callback'],
    ),
  );
  $default_items[] = $item;

  // Taxonomy.
  $item = array();
  $item['module'] = 'taxonomy';
  $item['groupheader'] = t('Taxonomy');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_taxonomy';

  // Get list of vocabularies.
  $vocabularies = array();
  if (function_exists('taxonomy_get_vocabularies')) {
    $vocabularies = taxonomy_get_vocabularies();
  }

  $sub_options = array();
  // Add items to sub-options.
  foreach ($vocabularies as $vkey => $v) {
    $sub_options['v' . $v->vid]['vid'] = $v->vid;
    $sub_options['v' . $v->vid]['type'] = 'vocabulary';
    $sub_options['v' . $v->vid]['title'] = $v->name . ' [' . $v->vid . ']';
    $sub_options['v' . $v->vid]['value'] = FALSE;
    $sub_options['v' . $v->vid]['class'] = 'vocabulary';
    $sub_options['v' . $v->vid]['style'] = 'margin-left: 15px;';
    $sub_options['v' . $v->vid]['parent'] = $item['batch_update_callback'];

    /* Get list of terms.
     * Get only the leaves, as deleting parents will cause the children to get
     * deleted too. Once the children is deleted, parents can be deleted.
     */
    $terms = site_cleanup_get_term_leaves($v->vid);
    if (empty($terms)) {
      $terms = taxonomy_get_tree($v->vid);
    }

    // Add items to sub-options.
    foreach ($terms as $tkey => $t) {
      $sub_options['v' . $v->vid . '_t' . $t->tid]['tid'] = $t->tid;
      $sub_options['v' . $v->vid . '_t' . $t->tid]['vid'] = $v->vid;
      $sub_options['v' . $v->vid . '_t' . $t->tid]['title'] = $t->name . ' [' . $t->tid . ']';
      $sub_options['v' . $v->vid . '_t' . $t->tid]['type'] = 'term';
      $sub_options['v' . $v->vid . '_t' . $t->tid]['value'] = TRUE;
      $sub_options['v' . $v->vid . '_t' . $t->tid]['class'] = 'term';
      $sub_options['v' . $v->vid . '_t' . $t->tid]['style'] = 'margin-left: 30px;';
      $sub_options['v' . $v->vid . '_t' . $t->tid]['parent'] = $item['batch_update_callback'] . '_v' . $v->vid;
    }
  }
  $item['sub_options'] = $sub_options;
  $default_items[] = $item;

  // Main menu.
  $item = array();
  $item['module'] = 'menu';
  $item['menu_name'] = 'main-menu';
  $item['groupheader'] = t('Main Menu');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_main_menu';
  $default_items[] = $item;

  // Secondary menu.
  $item = array();
  $item['module'] = 'menu';
  $item['menu_name'] = 'secondary-menu';
  $item['groupheader'] = t('Secondary Menu');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_secondary_menu';
  $default_items[] = $item;

  // Users.
  $item = array();
  $item['module'] = 'user';
  $item['groupheader'] = t('User');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_user';
  $default_items[] = $item;

  // Aliases.
  $item = array();
  $item['module'] = 'pathauto';
  $item['groupheader'] = t('Aliases');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_aliases';
  $default_items[] = $item;

  // Xmlsitemap.
  $item = array();
  $item['module'] = 'xmlsitemap';
  $item['groupheader'] = t('XML Sitemap');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_xmlsitemap';
  $default_items[] = $item;

  // Logs.
  $item = array();
  $item['module'] = 'log';
  $item['groupheader'] = t('Logs');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_logs';
  $default_items[] = $item;

  // Metatag.
  $item = array();
  $item['module'] = 'metatag';
  $item['groupheader'] = t('Metatags');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_metatag';
  $default_items[] = $item;

  // Dart.
  $item = array();
  $item['module'] = 'dart';
  $item['groupheader'] = t('Darts');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_dart';
  $default_items[] = $item;

  // Languages.
  $item = array();
  $item['module'] = 'i18n';
  $item['groupheader'] = t('Language');
  $item['batch_update_callback'] = 'site_cleanup_bulk_update_batch_process_language';
  $default_items[] = $item;

  return $default_items;
}
