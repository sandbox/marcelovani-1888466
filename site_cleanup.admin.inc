<?php
/**
 * @file
 * Admin page the site_cleanup module.
 */

require_once 'site_cleanup.inc';

/**
 * Form for bulk update form.
 */
function site_cleanup_bulk_update_form() {
  require_once 'site_cleanup.items.inc';

  $form['#update_callbacks'] = array();

  $form['update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select items to be removed. Use with caution, after this your site might look a bit empty.'),
  );

  // Load default settings.
  $site_cleanup_settings = module_invoke_all('site_cleanup_default_items', 'settings');

  foreach ($site_cleanup_settings as $settings) {
    $callback = $settings['batch_update_callback'];
    if (!empty($callback)) {

      $form['#update_callbacks'][$callback] = $settings;

      $form['update'][$callback] = array(
        '#type' => 'checkbox',
        '#title' => $settings['groupheader'],
        '#attributes' => array('class' => array('option')),
      );

      // Add sub-options.
      if (isset($settings['sub_options'])) {
        foreach ($settings['sub_options'] as $sub_option => $sub_option_item) {

          // Define parent to hide/show children options when selection items.
          if (isset($sub_option_item['parent'])) {
            $parent = $sub_option_item['parent'];
          }
          else {
            $parent = $callback;
          }

          $form['update'][$callback . '_' . $sub_option] = array(
            '#type' => 'checkbox',
            '#title' => $sub_option_item['title'],
            '#attributes' => array(
              'class' => array(
                (isset($sub_option_item['class']) ? $sub_option_item['class'] : 'sub-option'),
              ),
              'style' => array(
                (isset($sub_option_item['style']) ? $sub_option_item['style'] : 'margin-left: 15px;'),
              ),
            ),
            '#default_value' => $sub_option_item['value'],
            '#states' => array(
              'invisible' => array(
                'input[name="' . $parent . '"]' => array('checked' => FALSE),
              ),
            ),
          );
        }
      }
    }
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Form submit handler for path alias bulk update form.
 *
 * @see site_cleanup_batch_update_form()
 * @see site_cleanup_bulk_update_batch_finished()
 */
function site_cleanup_bulk_update_form_submit($form, &$form_state) {
  $batch = _site_cleanup_create_batch();

  foreach ($form['#update_callbacks'] as $callback => $item) {
    if ($form_state['values'][$callback]) {
      // Get settings.
      $settings = $form['#update_callbacks'][$callback];

      // Update sub-options with values submitted.
      foreach ($settings['sub_options'] as $sub_option => $sub_option_item) {
        if (isset($form_state['values'][$callback . '_' . $sub_option])) {
          $settings['sub_options'][$sub_option]['value'] = $form_state['values'][$callback . '_' . $sub_option];
        }
      }
      $batch['operations'][] = array(
        'site_cleanup_bulk_update_batch_process',
        array(
          $callback,
          $settings
        ),
      );
    }
  }
  batch_set($batch);
}
